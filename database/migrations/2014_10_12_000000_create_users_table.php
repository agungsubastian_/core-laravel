<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('users_id');
            $table->string('users_name');
            $table->string('users_email')->unique();
            $table->timestamp('users_email_verified_at')->nullable();
            $table->string('users_password');
            $table->rememberToken();
            $table->string('users_photo')->nullable();
            $table->string('users_api_token')->nullable();
            $table->tinyInteger('users_role', 0)->comment('0: superadmin, 1: admin')->unsigned();
            $table->boolean('users_status')->default(false);
            $table->boolean('users_is_companies')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
